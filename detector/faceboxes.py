# from .faceboxes.encoderl import DataEncoder

import torch
import numpy as np
from torch.autograd import Variable
import torch.nn.functional as F
import cv2
import time
font = cv2.FONT_HERSHEY_SCRIPT_SIMPLEX
import configparser
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
import itertools

class DataEncoder:
    def __init__(self):
        '''
        compute default boxes
        '''
        scale = 1024.
        sizes = [s / scale for s in (32, 256, 512)] 
        steps = [s / scale for s in (32, 64, 128)] # anchor's size.In the paper, the first output layer size is (32, 64, 128)
        aspect_ratios = ((1,2,4), (1,), (1,)) # more understanding
        feature_map_sizes = (32, 16, 8)


        density = [[-3,-1,1,3],[-1,1],[0]] # density for output layer1 -> n : 4 2 1
        
        num_layers = len(feature_map_sizes)
        boxes = []
        for i in range(num_layers):   ### 3 times
            fmsize = feature_map_sizes[i]
            for h,w in itertools.product(range(fmsize), repeat=2): ###   featuresize.pow(2)
                cx = (w + 0.5)/feature_map_sizes[i] # every grid value of centerX
                cy = (h + 0.5)/feature_map_sizes[i]

                s = sizes[i]
                for j,ar in enumerate(aspect_ratios[i]): ### 3 , 1, 1
                    if i == 0:
                        for dx,dy in itertools.product(density[j], repeat=2):   ### 21 = 4*4 + 2*2 + 1
                            boxes.append((cx+dx/8.*s/ar, cy+dy/8.*s*ar, s*ar, s/ar)) # add the offset
                    else:
                        boxes.append((cx, cy, s*ar, s/ar))
        
            
        self.default_boxes = torch.Tensor(boxes)

    def iou(self, box1, box2):
        '''Compute the intersection over union of two set of boxes, each box is [x1,y1,x2,y2].

        Args:
          box1: (tensor) bounding boxes, sized [N,4].
          box2: (tensor) bounding boxes, sized [M,4].

        Return:
          (tensor) iou, sized [N,M].
        '''
        N = box1.size(0)
        M = box2.size(0)

        lt = torch.max( # left top
            box1[:,:2].unsqueeze(1).expand(N,M,2),  # [N,2] -> [N,1,2] -> [N,M,2]
            box2[:,:2].unsqueeze(0).expand(N,M,2),  # [M,2] -> [1,M,2] -> [N,M,2]
        )

        rb = torch.min( # right bottom
            box1[:,2:].unsqueeze(1).expand(N,M,2),  # [N,2] -> [N,1,2] -> [N,M,2]
            box2[:,2:].unsqueeze(0).expand(N,M,2),  # [M,2] -> [1,M,2] -> [N,M,2]
        )

        wh = rb - lt  # [N,M,2]
        wh[wh<0] = 0  # clip at 0
        inter = wh[:,:,0] * wh[:,:,1]  # [N,M]

        area1 = (box1[:,2]-box1[:,0]) * (box1[:,3]-box1[:,1])  # [N,]
        area2 = (box2[:,2]-box2[:,0]) * (box2[:,3]-box2[:,1])  # [M,]
        area1 = area1.unsqueeze(1).expand_as(inter)  # [N,] -> [N,1] -> [N,M]
        area2 = area2.unsqueeze(0).expand_as(inter)  # [M,] -> [1,M] -> [N,M]

        iou = inter / (area1 + area2 - inter)
        return iou

    def nms(self, bboxes, scores, threshold=0.5):
        '''
        bboxes(tensor) [N,4]
        scores(tensor) [N,]
        '''
        x1 = bboxes[:,0]
        y1 = bboxes[:,1]
        x2 = bboxes[:,2]
        y2 = bboxes[:,3]
        areas = (x2-x1) * (y2-y1)

        _,order = scores.sort(0,descending=True)

        keep = []
        while order.numel() > 0:
            if order.numel() == 1:
                i = order.item()
                keep.append(i)
                break
            else:
                i = order[0].item()
                keep.append(i)

            xx1 = x1[order[1:]].clamp(min=x1[i])
            yy1 = y1[order[1:]].clamp(min=y1[i])
            xx2 = x2[order[1:]].clamp(max=x2[i])
            yy2 = y2[order[1:]].clamp(max=y2[i])

            w = (xx2-xx1).clamp(min=0)
            h = (yy2-yy1).clamp(min=0)
            inter = w*h

            ovr = inter / (areas[i] + areas[order[1:]] - inter)
            ids = (ovr<=threshold).nonzero().squeeze()
            if ids.numel() == 0:
                break
            order = order[ids+1]
        return torch.LongTensor(keep)

    def encode(self, boxes, classes, threshold=0.35):
        '''
        boxes:[num_obj, 4]
        default_box (x1,y1,w,h)
        return:boxes: (tensor) [num_obj,21824,4]
        classes:class label [obj,]
        '''
        boxes_org = boxes

        default_boxes = self.default_boxes  # [21824,4]

        iou = self.iou(
            boxes,
            torch.cat([default_boxes[:, :2] - default_boxes[:, 2:] / 2,  # (x0,y0,w,h) => (x1,y1,x2,y2)
                       default_boxes[:, :2] + default_boxes[:, 2:] / 2], 1))  # iou_size = (num_obj, 21824)

        # find max iou of each face in default_box
        max_iou, max_iou_index = iou.max(1)
        # find max iou of each default_box in faces
        iou, max_index = iou.max(0)

        # ensure every face have a default box,I think is no use.
        # max_index[max_iou_index] = torch.LongTensor(range(num_obj))

        boxes = boxes[max_index]  # [21824,4] 是图像label, use conf to control is or not.
        variances = [0.1, 0.2]
        cxcy = (boxes[:, :2] + boxes[:, 2:]) / 2 - default_boxes[:, :2]  # [21824,2]
        cxcy /= variances[0] * default_boxes[:, 2:]
        wh = (boxes[:, 2:] - boxes[:, :2]) / default_boxes[:, 2:]  # [21824,2]  为什么会出现0宽度？？
        wh = torch.log(wh) / variances[1]  # Variable

        inf_flag = wh.abs() > 10000
        if (inf_flag.sum().item() is not 0):
            print('inf_flag has true', wh, boxes)
            print('org_boxes', boxes_org)
            print('max_iou', max_iou, 'max_iou_index', max_iou_index)
            raise 'inf error'

        loc = torch.cat([cxcy, wh], 1)  # [21824,4]
        conf = classes[max_index]  # 1 [21824,]
        conf[iou < threshold] = 0  # iou
                                    # 与之对应的default_box，需要修改数据集里的label。

        return loc, conf

    def decode(self, loc, conf):
        '''
        loc [21842,4]
        conf [21824,2]
        '''
        variances = [0.1, 0.2]
        cxcy = loc[:,:2] * variances[0] * self.default_boxes[:,2:] + self.default_boxes[:,:2]
        wh  = torch.exp(loc[:,2:] * variances[1]) * self.default_boxes[:,2:]
        boxes = torch.cat([cxcy-wh/2,cxcy+wh/2],1) #[21824,4]
        
        # conf[:,0] means no face
        # conf[:,1] means face
        # filter face by a value of 0.4
        #conf[:,0] = 0.4

        max_conf, labels = conf.max(1) #[21842,1]

        if labels.long().sum().item() is 0:
            # no face in image
            return torch.tensor([0]), torch.tensor([0]), torch.tensor([0])

        ids = labels.nonzero().squeeze(1)
        # print('ids', ids)
        # print('boxes', boxes.size(), boxes[ids])

        keep = self.nms(boxes[ids],max_conf[ids])

        return boxes[ids][keep], labels[ids][keep], max_conf[ids][keep]


def init_model(m):
    if isinstance(m, nn.Conv2d):
        nn.init.xavier_uniform_(m.weight)
    elif isinstance(m, nn.BatchNorm2d):
        nn.init.constant_(m.weight, 1)
        nn.init.constant_(m.bias, 0)
    return m

class MultiBoxLayer(nn.Module):
    num_classes = 2
    num_anchors = [21,1,1]
    in_planes = [128,256,256]

    def __init__(self):
        super(MultiBoxLayer,self).__init__()
        
        self.loc_layers = nn.ModuleList()
        self.conf_layers = nn.ModuleList()
        for i in range(len(self.in_planes)):
            m0 = nn.Conv2d(self.in_planes[i], self.num_anchors[i] * 4, kernel_size=3, padding=1)
            m0 = init_model(m0)
            m1 = nn.Conv2d(self.in_planes[i], self.num_anchors[i] * 2, kernel_size=3, padding=1)
            m1 = init_model(m1)
            self.loc_layers.append(m0)
            self.conf_layers.append(m1)

    def forward(self,xs):
        '''
        xs:list of 之前的featuremap list
        retrun: loc_preds: [N,21824,4]
                conf_preds:[N,24824,2]
        '''
        y_locs=[]
        y_confs = []
        for i,x in enumerate(xs):
            y_loc = self.loc_layers[i](x) # N,anhors*4,H,W
            N = y_loc.size(0)
            y_loc = y_loc.permute(0,2,3,1).contiguous()
            y_loc = y_loc.view(N,-1,4)
            y_locs.append(y_loc)

            y_conf = self.conf_layers[i](x)
            y_conf = y_conf.permute(0,2,3,1).contiguous()
            y_conf = y_conf.view(N,-1,2)
            y_confs.append(y_conf)
            
        loc_preds = torch.cat(y_locs,1)
        conf_preds = torch.cat(y_confs,1)

        return loc_preds, conf_preds

def conv_bn_relu(in_channels,out_channels,kernel_size,stride=1,padding=0):
    m0 = nn.Conv2d(in_channels,out_channels,kernel_size=kernel_size,padding=padding,stride=stride)
    m0 = init_model(m0)
    m1 = nn.BatchNorm2d(out_channels)
    m1 = init_model(m1)
    return nn.Sequential(m0, m1, nn.ReLU(True))


class Inception(nn.Module):
    def __init__(self):
        super(Inception,self).__init__()
        self.conv1 = conv_bn_relu(128,32,kernel_size=1)
        self.conv2 = conv_bn_relu(128,32,kernel_size=1)
        self.conv3 = conv_bn_relu(128,24,kernel_size=1)
        self.conv4 = conv_bn_relu(24,32,kernel_size=3,padding=1)
        self.conv5 = conv_bn_relu(128,24,kernel_size=1)
        self.conv6 = conv_bn_relu(24,32,kernel_size=3,padding=1)
        self.conv7 = conv_bn_relu(32,32,kernel_size=3,padding=1)
    def forward(self,x):
        x1 = self.conv1(x)
        
        x2 = F.max_pool2d(x,kernel_size=3,stride=1,padding=1)
        x2 = self.conv2(x2)

        x3 = self.conv3(x)
        x3 = self.conv4(x3)
        
        x4 = self.conv5(x)
        x4 = self.conv6(x4)
        x4 = self.conv7(x4)

        output = torch.cat([x1,x2,x3,x4],1)
        return output


class FaceBox(nn.Module):
    input_size = 1024
    def __init__(self):
        super(FaceBox, self).__init__()

        #model
        self.conv1 = nn.Conv2d(3,24,kernel_size=7,stride=4,padding=3)
        self.conv1 = init_model(self.conv1)
        self.bn1 = nn.BatchNorm2d(24)
        self.bn1 = init_model(self.bn1)
        self.conv2 = nn.Conv2d(48,64,kernel_size=5,stride=2,padding=2)
        self.conv2 = init_model(self.conv2)
        self.bn2 = nn.BatchNorm2d(64)
        self.bn2 = init_model(self.bn2)

        self.inception1 = Inception()
        self.inception2 = Inception()
        self.inception3 = Inception()

        self.conv3_1 = conv_bn_relu(128,128,kernel_size=1)
        self.conv3_2 = conv_bn_relu(128,256,kernel_size=3,stride=2,padding=1)
        self.conv4_1 = conv_bn_relu(256,128,kernel_size=1)
        self.conv4_2 = conv_bn_relu(128,256,kernel_size=3,stride=2,padding=1)

        self.multilbox = MultiBoxLayer()


    def forward(self,x):
        hs = []
        
        x = self.conv1(x)
        x = self.bn1(x)
        x = F.relu(torch.cat((F.relu(x), F.relu(-x)),1))
        
        x = F.max_pool2d(x,kernel_size=3,stride=2,padding=1)
        x = self.conv2(x)
        x = self.bn2(x)
        x = F.relu(torch.cat((F.relu(x), F.relu(-x)),1))
        
        x = F.max_pool2d(x,kernel_size=3,stride=2,padding=1)
        x = self.inception1(x)
        x = self.inception2(x)
        x = self.inception3(x)
        hs.append(x)

        x = self.conv3_1(x)
        x = self.conv3_2(x)
        hs.append(x)

        x = self.conv4_1(x)
        x = self.conv4_2(x)
        hs.append(x)

        loc_preds, conf_preds = self.multilbox(hs)

        return loc_preds, conf_preds


class Faceboxes:
    def __init__(self, config_path):
        config = configparser.ConfigParser()
        config.read(config_path)

        self.model_path = config.get('faceboxes', 'model_path')
        self.threshold = 0.5
        self.image_width = int(config.get('faceboxes', 'image_width'))
        self.image_height = int(config.get('faceboxes', 'image_height'))
        self.detect_classes = config.get('faceboxes', 'detect_classes').split(',')
        self.use_gpu = int(config.get('faceboxes', 'use_gpu'))
        self.net = FaceBox()
        self.data_encoder = DataEncoder()
        self.net.load_state_dict(torch.load(self.model_path, map_location=lambda storage, loc:storage), strict=False) 
        self.net.eval()

    def inference(self, im, score_threshold=0.5):
        h, w, _ = im.shape
        dim_diff = np.abs(h - w)
        pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
        pad = ((pad1, pad2), (0, 0), (0, 0)) if h <= w else ((0, 0), (pad1, pad2), (0, 0))
        input_img = np.pad(im, pad, 'constant', constant_values=128)
        input_img = cv2.resize(input_img,(self.image_width, self.image_height))/255.

        im_tensor = torch.from_numpy(input_img.transpose((2,0,1))).float()

        if self.use_gpu:
            loc, conf = self.net(Variable(torch.unsqueeze(im_tensor, 0), volatile=True).cuda())
            loc, conf = loc.cpu(), conf.cpu()
        else:
            with torch.no_grad():
                loc, conf = self.net(torch.unsqueeze(im_tensor, 0))
        
        boxes, labels, probs = self.data_encoder.decode(loc.data.squeeze(0),
                                                    F.softmax(conf.squeeze(0), dim=1))
        to_keep = probs > score_threshold
        boxes = boxes[to_keep]
        probs = probs[to_keep]
        if probs[0] != 0:
            boxes = boxes.numpy()
            probs = probs.detach().numpy()
            if h <= w:
                boxes[:,1] = boxes[:,1]*w-pad1
                boxes[:,3] = boxes[:,3]*w-pad1
                boxes[:,0] = boxes[:,0]*w
                boxes[:,2] = boxes[:,2]*w
            else:
                boxes[:,1] = boxes[:,1]*h
                boxes[:,3] = boxes[:,3]*h
                boxes[:,0] = boxes[:,0]*h-pad1
                boxes[:,2] = boxes[:,2]*h-pad1

        return boxes, probs

    def detect_image(self, im, height, width, to_xywh, ratio_filter, confident_threshold=0.5):
        h,w,_ = im.shape
        boxes, probs = self.inference(im, confident_threshold)
        detection_results = []
        for i, (box) in enumerate(boxes):
            x = int(box[0])
            y = int(box[1])
            w = int(box[3]-box[1])
            h = int(box[2]-box[0])

            if h > w * ratio_filter:
                h = w * ratio_filter

            if w > 200:
                continue
            if x < 0 :
                w = w + x
                x = 0
            if y < 0 :
                h = h + y
                y = 0

            detection_results.append(['person', x, y, w, h, probs[i]])

        return detection_results
