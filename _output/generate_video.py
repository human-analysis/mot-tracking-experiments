import numpy as np
import json
from PIL import Image, ImageDraw
import os
import cv2
import pandas as pd
from tqdm import tqdm
import shutil
import random
from glob import glob

IMAGES_DIR = 'TA'

path = '*' #or '*'
# collect paths to all images
# image_paths = glob(os.path.join(IMAGES_DIR, path, 'img1', "*.jpg"))
# train_paths = []
# test_paths = []


def fill(temp):
    needed_zeros = 6 - len(temp)
    for i in range(needed_zeros):
        temp = '0' + temp
    return temp


def read_mot_results(filename, is_gt, is_ignore):
    global image_paths
    valid_labels = {1}
    ignore_labels = {2, 7, 8, 12}
    results_dict = dict()
    test_dict = dict()

    if os.path.isfile(filename):
        with open(filename, 'r') as f:
            for line in f.readlines():
                linelist = line.split(',')
                if len(linelist) < 7:
                    continue
                fid_ = linelist[0]
                if int(fid_) < 1:
                    continue

                fid = fill(fid_)
                if fid in filename:
                    continue
                # print(filename, '2222222')
                fid = '../2DMOT2015/train/' +  filename.split('/')[-1][:-4] + '/img1/' + fid + '.jpg'
                # print(fid, 2222222222)
                #                     print(fid_, mapping_gt[filename])

                #                     if fid in train_list:
                results_dict.setdefault(fid, list())
                #                     elif fid in test_list:
                #                         test_dict.setdefault(fid, list())

                if is_gt:
                    if 'MOT16-' in filename or 'MOT17-' in filename:
                        label = int(float(linelist[7]))
                        mark = int(float(linelist[6]))
                        if mark == 0 or label not in valid_labels:
                            continue
                    score = 1
                elif is_ignore:
                    if 'MOT16-' in filename or 'MOT17-' in filename:
                        label = int(float(linelist[7]))
                        vis_ratio = float(linelist[8])
                        if label not in ignore_labels and vis_ratio >= 0:
                            continue
                    else:
                        continue
                    score = 1
                else:
                    score = float(linelist[6])

                tlwh = tuple(map(float, linelist[2:6]))
                target_id = int(linelist[1])
                #                     if fid in train_list:
                results_dict[fid].append((tlwh, target_id, score))
    #                     elif fid in test_list:
    #                         test_dict[fid].append((tlwh, target_id, score))

    return results_dict

gt_paths = glob(os.path.join(IMAGES_DIR , '*', '*', '*.txt'))

for path in gt_paths:
    seq = path.split('/')[-1][:-4]
    saving_path = path[:-4]


# './_output/TA/mobilenetssd/skip1_with_prob_down_sample/file.txt'
def resize(oriimg):
    height, width, depth = oriimg.shape
    W = min(width, 1000.0)
    imgScale = W / width
    newX, newY = oriimg.shape[1] * imgScale, oriimg.shape[0] * imgScale
    newimg = cv2.resize(oriimg, (int(newX), int(newY)), interpolation=cv2.INTER_CUBIC)
    return newimg

import colorsys

def draw_boxes_on_image(path, item):
    hsv_tuples = [(x / 80, 1., 1.) for x in range(80)]
    colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    colors = list(map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))
    image = cv2.imread(path)
    #     image = resize(image)

    for b in item:
        xmin, ymin, w, h = b[0]
        xmax, ymax = xmin + w, ymin + h
        id = str(b[1])
        conf_str = str(b[2])

        fill = (255, 20, 255, 45)
        outline = 'red'
        label = ''
        cv2.rectangle(image, (int(xmin), int(ymin)), (int(xmax), int(ymax)), color=colors[7], thickness=3)
        label += id
        font = cv2.FONT_HERSHEY_COMPLEX_SMALL
        ret, baseline = cv2.getTextSize(label, fontFace=font, fontScale=0.5, thickness=1)
        #         cv2.rectangle(image, (int(xmin), int(ymin)), (int(xmax) + ret[0], int(ymax) + ret[1] + baseline), (0,0,0), thickness=1)
        cv2.putText(image, label, (int(xmin), int(ymin) + ret[1] + baseline - 4), font, 2.0, (255, 255, 0), 2)
    return resize(image)


def generator(list_txt):
    print(list_txt)
    for path in list_txt:
        #         saving_path = path.split('.')[0] + '.avi'
        saving_path = '.'
        a = read_mot_results(path, True, False)
        # if os.path.isfile(path[:-4] +'.avi'):
        #     continue
        try:
            height, width, depth = resize(cv2.imread(list(a.keys())[0])).shape
            fourcc = cv2.VideoWriter_fourcc(*'DIVX')
            # out = cv2.VideoWriter(path[:-4] +'.avi', fourcc, 25.0, (width, height))
            out = cv2.VideoWriter('output.avi', fourcc, 25.0, (width, height))

            for key in a.keys():
                image = draw_boxes_on_image(key, a[key])
                out.write(image)
        except:
            pass
        # out.close()


# generator(gt_paths)
# # img = cv2.imread('../2DMOT2015/train/ADL-Rundle-6/img1/000525.jpg')
# # print(img)
generator(['TA/yolov3_tiny/skip1_with_prob/ADL-Rundle-6.txt'])