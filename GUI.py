from __future__ import print_function
from PIL import Image
from PIL import ImageTk
import tkinter as tki
import PIL

from tkinter import filedialog, StringVar
import os
import errno
import sys
from timeit import time
import warnings
import cv2
import numpy as np
import datetime
import tkinter


from detector.detector import Detector
from tracker.tracker import Tracker_temp

warnings.filterwarnings('ignore')
IS_DETECTION_DISPLAY = False
IS_TRACKING_DISPLAY = True
ix, iy, ex, ey = -1, -1, -1, -1


def draw_rec(event, x, y, flags, param):
    global ix, iy, ex, ey, drawing, mode
    if event == cv2.EVENT_LBUTTONDOWN:
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        ex, ey = x, y
        cv2.rectangle(param, (ix, iy), (x, y), (0, 255, 0), 3)


track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                (0, 255, 255), (255, 0, 255), (255, 127, 255),
                (127, 0, 255), (127, 0, 127)]


def get_crop_size(cap, down_sample_ratio):
    while cap.isOpened():
        ret, frame = cap.read()
        (h, w) = frame.shape[:2]
        # frame = cv2.resize(frame, (int(w / down_sample_ratio), int(h / down_sample_ratio)))
        cv2.namedWindow('draw_rectangle')
        cv2.setMouseCallback('draw_rectangle', draw_rec, frame)
        print('Choose your area of interect!')
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('a'):
                cv2.destroyAllWindows()
                break
        break
    # print(ix, iy, ex, ey)


def testIntersectionIn(x, y):
    global ix, iy, ex, ey
    # a = iy - iy
    # b = iy
    res = y - iy
    if res > 1.0:
        return True
    return False


class tracking_by_detection(object):

    def __init__(self, detector_name='mobilenet_ssd', tracker_name='deep_sort', config_path='./config.cfg'):
        self.det = Detector(detector_name, config_path)
        self.tra = Tracker_temp(tracker_name, config_path)
        self.fps = 0.0
        self.step_counter = 0
        self.counter = 0
        self.first_time_flag = True
        self.start_time = time.time()
        self.total_time = time.time()
        self.result_list = []
        self.frame_index = 1
        self.textIn = 0
        self.textOut = 0
        # self.region = region

    def open_with_mkdir(self, path):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(os.path.dirname(path)):
                pass
            else:
                raise

        return open(path, 'w')


    def frame_processing(self, frame, detect_freq=1, down_sample_ratio=1.0,
                         is_probability_driven_detect=True, ratio_filter=1.0, location_variation=0.0,
                         confidence_increation=0.0, trace=True, print_fps=True, first_time_flag=True):
        global ix, iy, ex, ey
        (h, w) = frame.shape[:2]
        frame_resided = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
        if (self.step_counter % detect_freq == 0) or self.counter == 0 or (
                is_probability_driven_detect == True and self.tra.is_detection_needed() == True):
            results = self.det.detect_image_frame(frame_resided, to_xywh=True, ratio_filter=1.73)
            boxes = np.array([result[1:5] for result in results])
            for box in boxes:
                box[0] = max(0, box[0] - location_variation)
            if ratio_filter != 1.0:
                for box in boxes:
                    box[3] = min(box[3], box[2] * ratio_filter)
            boxes = np.array([box for box in boxes if box[2] < w / 3])
            scores = np.array([min(1.0, result[5] + confidence_increation) for result in results])
            self.tra.set_detecion_needed(False)

        tracker, detections = self.tra.start_tracking(frame_resided, boxes, scores)
        # Call the tracker
        masking = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.float)
        if (IS_TRACKING_DISPLAY is True):
            for track in tracker.tracks:
                if track.is_confirmed() and track.time_since_update > 1:
                    continue
                bbox = track.to_tlbr()
                w_, h_ = int((bbox[2] - bbox[0]) / down_sample_ratio), int((bbox[3] - bbox[1]) / down_sample_ratio)
                cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                              (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), (255, 255, 255),
                              2)
                # print(int(bbox[1] / down_sample_ratio),int(bbox[3] / down_sample_ratio),
                # #                 int(bbox[0] / down_sample_ratio), int(bbox[2] / down_sample_ratio))
                masking[int(bbox[1] / down_sample_ratio):int(bbox[3] / down_sample_ratio),
                                int(bbox[0] / down_sample_ratio): int(bbox[2] / down_sample_ratio)] += 0.01
                masking[int(bbox[1] / down_sample_ratio)+int(w_/8):int(bbox[3] / down_sample_ratio)-int(w_/8),
                                int(bbox[0] / down_sample_ratio)+int(h_/8): int(bbox[2] / down_sample_ratio)-int(h_/8)] += 0.01
                masking[int(bbox[1] / down_sample_ratio)+int(w_/4):int(bbox[3] / down_sample_ratio)-int(w_/4),
                                int(bbox[0] / down_sample_ratio)+int(h_/4): int(bbox[2] / down_sample_ratio)-int(h_/4)] += 0.01
                masking[int(bbox[1] / down_sample_ratio)+int(3*w_/8):int(bbox[3] / down_sample_ratio)-int(3*w_/8),
                                int(bbox[0] / down_sample_ratio)+int(3*h_/8): int(bbox[2] / down_sample_ratio)-int(3*h_/8)] += 0.01

                # masking[int(bbox[0] / down_sample_ratio): int(bbox[2] / down_sample_ratio),
                #                     int(bbox[1] / down_sample_ratio):int(bbox[3] / down_sample_ratio)] += 0.01
                # masking[int(bbox[0] / down_sample_ratio)+int(h_/8): int(bbox[2] / down_sample_ratio)-int(h_/8),
                #                 int(bbox[1] / down_sample_ratio)+int(w_/8):int(bbox[3] / down_sample_ratio)-int(w_/8)] += 0.01
                # masking[int(bbox[0] / down_sample_ratio)+int(h_/4): int(bbox[2] / down_sample_ratio)-int(h_/4),
                #                 int(bbox[1] / down_sample_ratio)+int(w_/4):int(bbox[3] / down_sample_ratio)-int(w_/4)] += 0.01
                # masking[int(bbox[0] / down_sample_ratio)-int(3*h_/8): int(bbox[2] / down_sample_ratio)-int(3*h_/8),
                #                 int(bbox[1] / down_sample_ratio)+int(3*w_/8):int(bbox[3] / down_sample_ratio)-int(3*w_/8)] += 0.01
                cv2.putText(frame, str(track.track_id),
                            (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)), 0, 5e-3 * 200,
                            (0, 255, 0), 2)
                # bbox = track.to_tlwh()
                centroid = (bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2
                text = "ID {}".format(track.track_id)
                # cv2.putText(frame, text, (int(centroid[0] - 10), int(centroid[1] - 10)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                # cv2.putText(frame, str(track.track_id),
                #             (int((centroid[1]) / down_sample_ratio), int((centroid[0])/down_sample_ratio)), 0, 5e-3 * 200,
                #             (0, 255, 0), 2)
                # cv2.circle(frame, (int(centroid[0]), int(centroid[1])), 4, (0, 255, 0), -1)
                # cv2.putText(frame, str(int(centroid[1])),(int(centroid[0]), int(centroid[1])),0, 5e-3 * 200, (0,255,0),2)
                if trace and len(track.prev_box) >= 2:
                    for i in range(0, len(track.prev_box) - 1):
                        # Draw trace line
                        x1 = (track.prev_box[i][0] + track.prev_box[i][2]) / 2 / down_sample_ratio
                        y1 = (track.prev_box[i][1] + track.prev_box[i][3]) / 2 / down_sample_ratio
                        x2 = (track.prev_box[i + 1][0] + track.prev_box[i + 1][2]) / 2 / down_sample_ratio
                        y2 = (track.prev_box[i + 1][1] + track.prev_box[i + 1][3]) / 2 / down_sample_ratio
                        clr = track.track_id % 9
                        cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                 track_colors[clr], 2)

                self.result_list.append(
                    [self.frame_index, track.track_id, bbox[0] / down_sample_ratio, bbox[1] / down_sample_ratio,
                     bbox[2] / down_sample_ratio, bbox[3] / down_sample_ratio])

        for track in tracker.tracks:
            if track.is_confirmed() and track.time_since_update > 1:
                continue
            bbox = track.to_tlbr()
            centroid = (bbox[0] + bbox[2]) / 2 / down_sample_ratio, (bbox[1] + bbox[3]) / 2 / down_sample_ratio
            text = "ID {}".format(track.track_id)
            cv2.circle(frame, (int(centroid[0]), int(centroid[1])), 4, (0, 255, 0), -1)
            # cv2.putText(frame, str(int(centroid[1])), (int(centroid[0]), int(centroid[1])), 0, 5e-3 * 200,
            #             (0, 255, 0), 2)
            if len(track.to_centroids()):
                y = [c[1] for c in track.to_centroids()]
                orientation = centroid[1] - np.mean(y) - 0
                # print(orientation, centroid, ey)
                if not track.counted:
                    if orientation > 1.0 and (centroid[1] <= ey and centroid[1] > iy):
                        self.textIn += 1
                        track.counted = True
                    elif orientation < 1.0 and (centroid[1] >= iy and centroid[1] < ey):
                        self.textOut += 1
                        track.counted = True
        #                         print(len(track.to_centroids()))
        # if len(track.to_centroids()) > 2:
        #     current_x, current_y = track.to_centroids()[-1]
        #     prev_x, prev_y = track.to_centroids()[-2]aq
        #     # print(prev_x, prev_y)
        #     #                           print(len(track.to_centroids()))
        #     cv2.line(frame, (int(prev_x), int(prev_y)),
        #              (int(current_x), int(current_y)), (0, 255, 0), 3)

        # print(self.textIn, self.textOut)
        # labels = [[[result[1], result[2]], [result[1], result[4]], [result[3], result[4]], [result[3], result[2]]] for
        #           result in results]
        # cs = [[[detection.to_tlbr()[0], detection.to_tlbr()[1]],[detection.to_tlbr()[0], detection.to_tlbr()[3]],
        #        [detection.to_tlbr()[2], detection.to_tlbr()[3]], [detection.to_tlbr()[2], detection.to_tlbr()[1]]]
        #          for detection in detections]
        if (IS_DETECTION_DISPLAY is True):
            for detection in detections:
                bbox = detection.to_tlbr()
                cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                              (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), (255, 0, 0), 2)


        self.counter += 1
        self.step_counter += 1
        fps = 0
        if (self.step_counter % detect_freq == 0):
            fps = self.step_counter / (time.time() - self.start_time)
            if (print_fps is True):
                print(fps)
            self.step_counter = 0
            cv2.putText(frame, 'FPS:' + str(round(fps, 1)), (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            self.start_time = time.time()
            if (first_time_flag is True):
                self.step_counter = 0
                self.counter = 0
                total_time = time.time()
                first_time_flag = False

        cv2.line(frame, (int(ix), int(iy)), (int(ex), int(iy)), (0, 255, 0), 2)
        cv2.putText(frame, str(int(iy)), (int(ix), int(iy)), 0, 5e-3 * 200, (0, 255, 0), 2)

        cv2.line(frame, (int(ix), int(ey)), (int(ex), int(ey)), (255, 0, 0), 2)
        cv2.putText(frame, str(int(ey)), (int(ix), int(ey)), 0, 5e-3 * 200, (255, 0, 0), 2)

        # font = cv2.FONT_HERSHEY_COMPLEX_SMALL
        # left = 0
        # top = 0
        # ret, baseline = cv2.getTextSize('Leaver: ' + str(self.textOut), fontFace=font, fontScale=1.3, thickness=1)
        # cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
        #               color=(122, 255, 33), thickness=-1)
        # cv2.putText(frame, 'Leaver: ' + str(self.textOut), (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1,
        #             cv2.LINE_AA)
        #
        # text = 'Inner: ' + str(self.textIn)
        # x = 30
        # y = 30
        # i = 1
        #
        # left = 0
        # top = y
        # cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
        #               color=(255, 255, 255), thickness=-1)
        # cv2.putText(frame, text, (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)
        cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                    (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
        # if (show_image == True):
        #     cv2.imshow('image', frame)
        self.frame_index += 1
        # for label in cs:
        #     masking[label[0][1]:label[2][1], label[0][0]:label[2][0]] += 1
        # if (show_image == True):
        # cv2.destroyAllWindows()
        return frame, self.textIn, self.textOut, fps, masking


class PhotoBoothApp:
    def __init__(self, outputPath):
        # store the video stream object and output path, then initialize
        # the most recently read frame, thread for reading frames, and
        # the thread stop event
        self.root = tki.Tk()
        self.outputPath = outputPath

        self.canvas = None
        btn = tki.Button(self.root, text="Select a video", command=self.initializer)
        btn.pack(anchor=tkinter.CENTER, expand=True)

        self.root.mainloop()

    def initializer(self):

        path = filedialog.askopenfilename()
        if path.endswith('mp4'):
            self.vs = cv2.VideoCapture(path)
            get_crop_size(self.vs, 1.0)
            time.sleep(2.0)
            self.frame = None
            self.thread = None
            self.stopEvent = None
            self.tra_by_det = tracking_by_detection()

            self.panel = None

            self.btn_snapshot = tkinter.Button(self.root, text="Snapshot", width=50, command=self.snapshot)
            self.btn_snapshot.pack(anchor=tkinter.CENTER)

            self.canvas = tkinter.Canvas(self.root, width=self.vs.get(3) + 100, height=self.vs.get(4))
            self.canvas.pack()

            self.w1 = tki.Label(self.root, text='')
            self.w2 = tki.Label(self.root, text='')

            self.delay = 15
            self.videoLoop()

        # self.root.mainloop()

    def videoLoop(self):
        ret, self.frame = self.vs.read()
        if ret:
            start = time.time()
            self.frame, textIn, textOut = self.tra_by_det.frame_processing(frame=self.frame,print_fps=False)
            self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(self.frame))
            self.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)

            # Inner_text = StringVar()
            # Leaver_text = StringVar()

            self.w1.pack()
            self.w1['text'] = '{}'.format(textIn)
            self.w2.pack()
            self.w2['text'] = '{}'.format(textOut)


        self.root.after(self.delay, self.videoLoop)

    def snapshot(self):
        ret, frame = self.vs.get_frame()
        if ret:
            cv2.imwrite("frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))

    def onClose(self):
        print("[INFO] closing...")
        self.stopEvent.set()
        self.vs.release()
        cv2.destroyAllWindows()
        self.root.quit()


# pba = PhotoBoothApp('./result')
