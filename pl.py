import tkinter as tk
from tkinter import *
from tkinter import filedialog
import sys, Pmw
from PIL import Image, ImageTk, ImageEnhance
import cv2
from GUI import tracking_by_detection, get_crop_size
import time
from sklearn.neighbors import KernelDensity
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib.pylab import cm


def kde2D(x, y, xbins=100j, ybins=100j, kde_skl=None):
    """Build 2D kernel density estimate (KDE)."""

    # create grid of sample locations (default: 100x100)
    xx, yy = np.mgrid[x.min():x.max():xbins,
                      y.min():y.max():ybins]

    xy_sample = np.vstack([yy.ravel(), xx.ravel()]).T
    xy_train  = np.vstack([y, x]).T

    # kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(xy_train)

    # score_samples() returns the log-likelihood of the samples
    z = np.exp(kde_skl.score_samples(xy_sample)+2)
    return xx, yy, np.reshape(z, xx.shape)



class MainApplication():
    def __init__(self, master=tk.Tk(), bandwidth=130.0, **kwargs):
        master.geometry("650x520")
        self.kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
        self.accumulated_exposures = None
        self.size = None
        self.frame_rate = 0.4
        self.down_sample_ratio = 1.0
        self.master = master
        self.path_video = "/media/tuanhle/D466DA6A66DA4CBC/pycharm/multiple_object_tracking/human_detection/mot_deepsort/ezgif.com-video-cutter.mp4"
        self.cap = cv2.VideoCapture(self.path_video)
        self.number_frame = 0

        detector_name = 'mobilenet_ssd'
        tracker_name = 'deep_sort'

        self.tra_by_det = tracking_by_detection(detector_name, tracker_name)

        self.kde = KernelDensity(bandwidth=0.3, metric='haversine',
                            kernel='gaussian', algorithm='ball_tree')

        menubar = Menu(self.master)
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open", command=self.file_open)
        filemenu.add_command(label="Save", command=self.start)
        filemenu.add_command(label="Save as...", command=self.start)
        filemenu.add_separator()

        filemenu.add_command(label="Exit", command=self.master.quit)
        menubar.add_cascade(label="File", menu=filemenu)

        helpmenu = Menu(menubar, tearoff=0)
        helpmenu.add_command(label="About...", command=self.start)
        menubar.add_cascade(label="Help", menu=helpmenu)

        self.master.config(menu=menubar)
        self.bnt_tracking_start = Button(self.master, text="Start", command=self.start)
        self.bnt_tracking_start.place(x=0, y=0)

        # print(self.bnt_tracking_start.winfo_width())

        self.trace_flag = False
        self.bnt_tracking_trace = Button(self.master, text="Trace", command=self.trace_fn)
        self.bnt_tracking_trace.place(x=59, y=0)

        self.bnt_drawing_line = Button(self.master, text="Choose your margin line", command=self.draw)
        self.bnt_drawing_line.place(x=123, y=0)

        self.heatmap_flag = False
        self.bnt_tracking_trace = Button(self.master, text="HeatMap", command=self.heatmap_fn)
        self.bnt_tracking_trace.place(x=310, y=0)

        self.info_text = tk.StringVar(self.master)
        self.info_label = Label(self.master, textvariable=self.info_text)
        self.info_text.set('Inner:  {}'.format(0) + '\tLeaver:   {}'.format(0) + '   FPS:   {}'.format(0))
        self.info_label.place(x=5, y=30)

        self.mainFrame = Frame(self.master)
        self.mainFrame.place(x=0, y=50)

        self.video_frame = Label(self.mainFrame)
        self.video_frame.grid(row=0, column=0)

        self.video_frame.grid_rowconfigure(0, weight=2)
        self.video_frame.grid_columnconfigure(0, weight=2)

    def draw(self):
        get_crop_size(self.cap, self.down_sample_ratio)

    def heatmap_fn(self):
        self.heatmap_flag = True

    def trace_fn(self):
        self.trace_flag = True

    def new_frame(self, event=None):
        self.number_frame += 1
        new = tk.Toplevel(self.master)
        new.title("{}".format(self.number_frame))
        return new

    # def show_frame(self):
    #     ret, frame = cap
    @staticmethod
    def heatmap(cs, image, ax, fig, cmap, kde):
        h, w, _ = image.shape
        cs[:, 1] = h - cs[:, 1]
        x, y = cs[:, 0], cs[:, 1]

        xx, yy, zz = kde2D(x, y, kde)

        levels = MaxNLocator(hot=15).tick_values(zz.min(), zz.max())
        norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

        ax.pcolormesh(xx, yy, zz, cmap=cmap, norm=norm)
        # ax.scatter(x, y, s=2, facecolor='white')
        fig.canvas.draw()

        # Now we can save it to a numpy array.
        data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))


        img = cv2.addWeighted(data, 0.5, image, 1, gamma=0)
        return img

    def new_tk_image(self, frame, cs=None):
        # print(frame.shape)
        # if cs:
        #     # print(11111111111111111111111)
        #     # print(self.ax, self.fig, self.cmap)
        #     frame = self.heatmap(cs, frame, self.ax, self.fig, self.cmap, self.kde_skl)
        #     print(frame.shape)

        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        image = Image.fromarray(frame)
        imageTk = ImageTk.PhotoImage(image=image)
        return imageTk

    @staticmethod
    def highlight_labels(img, labels, maskimg=None):
        # create a copy of the image so we can draw on it
        imgcpy = img.copy()

        # draw a quadrilateral for each label in red
        cv2.polylines(imgcpy, labels, True, (0, 0, 255), thickness=2)

        # a mask needs to be created from the labels so we can properly highlight.
        # if the mask isn't passed in, we create it
        if type(maskimg) == type(None):
            maskimg = np.zeros(imgcpy.shape, dtype=np.float)
            for label in labels:
                # this sets all pixels inside the label to 1
                cv2.fillConvexPoly(maskimg, label, (1))

                # create a rgb version of the mask by setting each channel to the mask we created
        maskimg = (maskimg > 0).astype(np.uint8)
        maskrgb = np.zeros(imgcpy.shape, np.uint8)
        maskrgb[:, :, 0] = maskimg
        maskrgb[:, :, 1] = maskimg
        maskrgb[:, :, 2] = maskimg

        # interpolate image with white using a weighted sum
        bgimg = .5 * 255 * np.ones(imgcpy.shape, np.float) + .5 * imgcpy.astype(np.float)
        # mask out the background
        bgimg = (1 - maskrgb) * bgimg
        # cast to uint8 image
        bgimg = np.round(bgimg).astype(np.uint8)

        # get foreground unchanged
        fgimg = maskrgb * imgcpy

        # add white tinted background with unchanged foreground
        imgcpy = bgimg + fgimg.astype(np.uint8)

        return imgcpy

    def show_image(self):
        try:
            ret, frame = self.cap.read()
            frame, textIn, textOut, fps, masking = self.tra_by_det.frame_processing(frame, down_sample_ratio=self.down_sample_ratio,
                                                                           trace=self.trace_flag, print_fps=False)
            # print(cs)
            # labels = np.array([[[result[1], result[2]], [result[1], result[4]], [result[3], result[4]], [result[3], result[2]]]
            #           for result in cs])
            # print("=" * 100)
            if type(self.accumulated_exposures) == type(None):
                self.accumulated_exposures = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.float)
            # print(self.accumulated_exposures.shape)
            # masking = np.zeros(self.accumulated_exposures.shape, dtype=np.float)
            # print(labels.shape)
            # for label in labels:
            #     cv2.fillConvexPoly(masking, label, (1.0))
            self.accumulated_exposures = self.accumulated_exposures + masking
            # print(self.accumulated_exposures)
            if self.heatmap_flag:
                canvas = np.uint8(cm.jet(self.accumulated_exposures) * 255)
                canvas = cv2.cvtColor(canvas, cv2.COLOR_RGBA2BGR)
                # cv2.imwrite("heatmap.png", canvas)
                # print(canvas)
                img = cv2.addWeighted(canvas, 0.2, frame, 0.9, gamma=0)
                cv2.imshow('image', img)
                k = chr(cv2.waitKey())
                if k == 'q':
                    cv2.destroyAllWindows()
                self.heatmap_flag = False
            # print(frame.shape)
            self.info_text.set('Inner:  {}'.format(textIn) + '\tLeaver:   {}'.format(textOut) + '   FPS:   {}'.format(round(fps, 2)))
            # cs = None if self.heatmap_flag is False else cs
            imageTk = self.new_tk_image(frame, cs=None)
            self.video_frame.imgtk = imageTk
            self.video_frame.configure(image=imageTk)
            self.video_frame.after(1, self.show_image)
            # video_frame.after(1)
        except:
            pass

    
    def start(self):
        if self.path_video is None:
            new_frame = self.new_frame()
            tmp_label = tk.Label(new_frame, padx=40, pady=40, text="here is the label")
            tmp_label.grid()
        else:
            self.show_image()

    def file_open(self):
        self.path_info = filedialog.askopenfile()
        self.path_video = self.path_info.name
        self.cap = cv2.VideoCapture(self.path_video)
        w, h = self.cap.get(3), self.cap.get(4)
        self.fig = plt.figure(figsize=(w / 100, (h + 1) / 100))
        # fig(figsize=(w, h))
        mpl.rcParams['savefig.pad_inches'] = 0
        self.ax = plt.axes([0, 0, 1, 1], frameon=False)
        self.ax.get_xaxis().set_visible(False)
        self.ax.get_yaxis().set_visible(False)
        plt.autoscale(tight=True)
        self.cmap = plt.get_cmap('autumn')
        if self.size:
            self.down_sample_ratio = w / self.size[0]
        # self.master.geometry("{}x{}".format(w, h+50))
        # print(self.path_video.name)
        # print(self.path_video)

if __name__ == '__main__':
    App = MainApplication()
    # App.pack(side='top', fill='both', expand=True)
    App.master.mainloop()