#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import os
import errno
from timeit import time
from PIL import Image
import warnings
import cv2
import numpy as np
import argparse
import threading
import queue
import colorsys

import matplotlib.pyplot as plt 
# from utils.bounding_box_transfor
# im import to_tlwh

from detector.detector import Detector
from tracker.tracker import Tracker_temp

warnings.filterwarnings('ignore')
IS_DETECTION_DISPLAY = True
IS_TRACKING_DISPLAY = False



class tracking_by_detection(object):
    # cap_from_stream: bool
    ix, iy, ex, ey = -1, -1, -1, -1
    hsv_tuples = [(x / 80, 1., 1.)
                  for x in range(80)]
    colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    colors = list(
        map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))

    def __init__(self, detector_name, tracker_name, is_crop = True, config_path='./config.cfg'):
        self.det = Detector(detector_name, config_path)
        self.tra = Tracker_temp(tracker_name, config_path)
        self.frame_sz = (1280, 720)
        self.is_crop = is_crop
        self.running = True
        self.q = queue.Queue()
        self.cap_from_stream = False

    def open_with_mkdir(self, path):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(os.path.dirname(path)):
                pass
            else:
                raise

        return open(path, 'w')

    @staticmethod
    def draw_rec(event, x, y, flags, param):
        # global ix, iy, ex, ey, drawing, mode

        if event == cv2.EVENT_LBUTTONDOWN:
            tracking_by_detection.ix, tracking_by_detection.iy = x, y

        elif event == cv2.EVENT_LBUTTONUP:
            tracking_by_detection.ex, tracking_by_detection.ey = x, y
            cv2.line(param, (tracking_by_detection.ix, tracking_by_detection.iy), (x, y), (0, 255, 0), 3)

    def get_crop_size(self, video_path):
        cap = cv2.VideoCapture(video_path)
        while cap.isOpened():
            ret, frame = cap.read()
            if self.cap_from_stream:
                frame = cv2.resize(frame, self.frame_sz)
            cv2.namedWindow('draw_rectangle')
            cv2.setMouseCallback('draw_rectangle', self.draw_rec, frame)
            print('Choose your area of interect!')
            while 1:
                cv2.imshow('draw_rectangle', frame)
                k = cv2.waitKey(1) & 0xFF
                if k == ord('a'):
                    cv2.destroyAllWindows()
                    break
            break


    def grab(self, cam, queue, delay):

        capture = cv2.VideoCapture(cam)
        now = time.time()

        if type(cam) == int:
            while self.running:
                fr = {}
                capture.grab()
                retval, img = capture.read()
                if not retval:
                    self.running = False
                fr["img"] = img
        else:
            while self.running:
                fr = {}
                retval, img = capture.read()
                if not retval:
                    self.running = False
                fr["img"] = img
                cur = time.time()
                if cur - now >= delay:
                    queue.put(fr)
                    now = cur

    def tracking_by_detection(self, video_stream, output_file, show_image=False, detect_freq=4, down_sample_ratio=0.5,
                              is_probability_driven_detect=True, print_fps=False):
        if self.is_crop:
            self.get_crop_size(video_stream)
            print('Your area of interest: ', tracking_by_detection.ix, ' ', tracking_by_detection.iy, ' ', tracking_by_detection.ex, ' ',tracking_by_detection.ey)
        area = (tracking_by_detection.ix, tracking_by_detection.iy, tracking_by_detection.ex, tracking_by_detection.ey)
        S_ROI = (tracking_by_detection.ex - tracking_by_detection.ix + 1) * (tracking_by_detection.ey - tracking_by_detection.iy + 1)
        print(area,' ', S_ROI)
        video_capture = cv2.VideoCapture(video_stream)
        w = int(video_capture.get(3))
        h = int(video_capture.get(4))
        if self.cap_from_stream:
            w_ = self.frame_sz[0]
            h_ = self.frame_sz[1]
        video_fps = video_capture.get(cv2.CAP_PROP_FPS)
        human_counter = 0
        meter_per_pixel = 0.05
        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        # out = cv2.VideoWriter(path[:-4] +'.avi', fourcc, 25.0, (width, height))
        hsv_tuples = [(x / 80, 1., 1.)
              for x in range(80)]
        colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))
        out = cv2.VideoWriter('output.avi', fourcc, video_fps, (w, h))
        fps = 0.0
        step_counter = 0
        counter = 0
        fig = plt.figure(figsize=(6, 6))
        ax = fig.add_subplot(1, 1, 1)
        first_time_flag = True
        start_time = time.time()
        total_time = time.time()

        capture_thread = threading.Thread(target=self.grab, args=(video_stream, self.q, 0))
        capture_thread.start()

        result_list = []
        frame_index = 1
        while True:
            try:
                fr = self.q.get()
                frame = fr['img']
                # print(frame.shape)
                # (h, w) = frame.shape[:2]
                human_counter = 0
                frame = cv2.resize(frame, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
                # frame = Image.fromarray(frame)

                if ((step_counter % detect_freq == 0) or counter == 0 or (
                        is_probability_driven_detect == True and self.tra.is_detection_needed() == True)):
                    results = self.det.detect_image_frame(frame, to_xywh=True)
                    # print(111111111111)
                    boxes = np.array([result[1:5] for result in results])
                    scores = np.array([result[5] for result in results])
                    self.tra.set_detecion_needed(False)


                # print(h, w)
                tracker, detections = self.tra.start_tracking(frame, boxes, scores)
                # Call the tracker

                if IS_TRACKING_DISPLAY is True:
                    for track in tracker.tracks:
                        if track.is_confirmed() and track.time_since_update > 1:
                            continue
                        human_counter += 1
                        bbox = track.to_tlbr()
                        cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                      (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), (255, 255, 255),
                                      2)
                        cv2.putText(frame, str(track.track_id),
                                    (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)), 0, 5e-3 * 200,
                                    (0, 255, 0), 2)
                        bbox = track.to_tlwh()
                        result_list.append(
                            [frame_index, track.track_id, bbox[0] / down_sample_ratio, bbox[1] / down_sample_ratio,
                             bbox[2] / down_sample_ratio, bbox[3] / down_sample_ratio])

                if IS_DETECTION_DISPLAY is True:
                    for detection in detections:
                        bbox = detection.to_tlbr()
                        cv2.rectangle(frame, (int(bbox[0] / down_sample_ratio), int(bbox[1] / down_sample_ratio)),
                                      (int(bbox[2] / down_sample_ratio), int(bbox[3] / down_sample_ratio)), color=tracking_by_detection.colors[7], thickness=2)
                # print(counter)
                counter += 1
                step_counter += 1
                if step_counter % detect_freq == 0:
                    fps = step_counter / (time.time() - start_time)
                    if print_fps is True:
                        print(fps)
                    step_counter = 0
                    cv2.putText(frame, 'FPS:' + str(round(fps, 1)), (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    start_time = time.time()
                    if first_time_flag is True:
                        step_counter = 0
                        counter = 0
                        total_time = time.time()
                        first_time_flag = False
                # print(frame.shape)
                ax.clear()
                ax.set_ylim(bottom=0, top=50)
                ax.set_xlim(left=0, right=300)
                ax.set_xlabel('Timer (s)')
                ax.set_ylabel('Number of person')
                handles, labels = ax.get_legend_handles_labels()
                ax.legend(handles, labels)
                fig.canvas.draw()
                img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
                img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
                img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                img = cv2.resize(img, (int(w * down_sample_ratio), int(h * down_sample_ratio)))
                # cv2.imshow("plot", img)

                if human_counter <= 25.:
                    status = 'Normal'
                    color_status = (255, 255, 255)
                elif human_counter <= 50.:
                    status = 'Crowd'
                    color_status = (255, 0, 0)
                else:
                    status = 'Very crowd'
                    color_status = (0, 0, 255)

                font = cv2.FONT_HERSHEY_COMPLEX_SMALL
                text = 'Density: %.2f %%. Status: %s' % (human_counter, status)
                left = 30
                top = self.frame_sz[1] - 50
                ret, baseline = cv2.getTextSize(text, fontFace=font, fontScale=1.3, thickness=1)
                cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                          color=color_status, thickness=-1)
                cv2.putText(frame, text, (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1,
                            cv2.LINE_AA)

                left = 0
                top = 0
                ret, baseline = cv2.getTextSize('person', fontFace=font, fontScale=1.3, thickness=1)
                cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                              color=colors[0], thickness=-1)
                cv2.putText(frame, 'person', (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1,
                            cv2.LINE_AA)

                text = str(human_counter)
                x = 30
                y = 30
                i = 1

                left = 0
                top = y
                cv2.rectangle(frame, (left, top), (left + ret[0], top + ret[1] + baseline),
                              color=(255, 255, 255), thickness=-1)
                cv2.putText(frame, text, (left, top + ret[1] + baseline), font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)

                # cv2.line(frame, (tracking_by_detection.ix, tracking_by_detection.iy),
                #               (tracking_by_detection.ex, tracking_by_detection.ey), (0, 255, 0), 3)
                cv2.line()
                if show_image:
                    # print(frame.shape)
                    cv2.imshow('image', frame)
                # return 
                frame_index += 1
                if self.cap_from_stream:
                    frame = cv2.resize(frame, self.frame_sz)
                out.write(frame)

                k = cv2.waitKey(1) & 0xff
                if k == ord('n'):
                    continue
                elif k == 27:  # 'esc' key has been pressed, exit program.
                    self.running = False
                    break
            except Exception as inst:
                print(inst)
                pass

        out.release()
        video_capture.release()
        cv2.destroyAllWindows()

        fps = counter / (time.time() - total_time)
        print('Average FPS:', round(fps, 1))
        print('Total eplased:', round(time.time() - total_time, 2))

def main():
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''
    Command line options
    '''
    parser.add_argument('--detector', type=str, help='type of model for detection(yolo, mobileNet, Squeeze', default='new_mobilenet_ssd')
    parser.add_argument('--tracker', type=str, help='type of tracker .', default='deep_sort')
    parser.add_argument('--input', type=str, help='path to your test video.', default='Pedestrian_detector.mp4')
    parser.add_argument('--output', type=str, help='path to your save video', default='./')
    # parser.add_argument('-show', help='If you want show you online or save you video in your path')
    FLAGS = parser.parse_args()
    if FLAGS.input:
        print('Video tracking mode')
        tra_by_det = tracking_by_detection(FLAGS.detector, FLAGS.tracker)
        tra_by_det.tracking_by_detection(video_stream=FLAGS.input,
                                         output_file=FLAGS.input,
                                         show_image=True, detect_freq=2, down_sample_ratio=1.0,
                                         is_probability_driven_detect=True)

def gui(input, detector='mobilenet_ssd', tracker='deep_sort', output='./'):
    if input:
        print('Video tracking mode')
        tra_by_det = tracking_by_detection(detector, tracker)
        tra_by_det.tracking_by_detection(video_stream=input,
                                         output_file=input,
                                         show_image=False, detect_freq=3, down_sample_ratio=1.0,
                                         is_probability_driven_detect=True)

if __name__ == "__main__":
    main()