from detector.detector import Detector
import configparser
import math
import time
import numpy as np
import cv2
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import LeaveOneOut

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib.pylab import cm
import os

def kde2D(x, y, bandwidth, xbins=100j, ybins=100j, **kwargs):
    """Build 2D kernel density estimate (KDE)."""

    # create grid of sample locations (default: 100x100)
    xx, yy = np.mgrid[x.min():x.max():xbins,
                      y.min():y.max():ybins]

    xy_sample = np.vstack([yy.ravel(), xx.ravel()]).T
    xy_train  = np.vstack([y, x]).T

    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(xy_train)

    # score_samples() returns the log-likelihood of the samples
    z = np.exp(kde_skl.score_samples(xy_sample))
    return xx, yy, np.reshape(z, xx.shape)

def heatmap(cs, image):
    h, w, _ = image.shape
    cs[:, 1] = h - cs[:, 1]
    x, y = cs[:, 0], cs[:, 1]

    xx, yy, zz = kde2D(x, y, 130.0)

    fig = plt.figure(figsize=(w/100, (h+1)/100))
    # fig(figsize=(w, h))
    mpl.rcParams['savefig.pad_inches'] = 0
    ax = fig.add_subplot(111)
    ax = plt.axes([0,0,1,1], frameon=False)
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.autoscale(tight=True)
    levels = MaxNLocator(hot=15).tick_values(zz.min(), zz.max())
    cmap = plt.get_cmap('autumn')
    norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

    ax.pcolormesh(xx, yy, zz, cmap=cmap, norm=norm)
    # ax.scatter(x, y, s=2, facecolor='white')
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    fig.canvas.draw()

    # Now we can save it to a numpy array.
    data = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    data = data.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    print(data.shape, image.shape, type(data), type(image)
          , '------------------------')

    img = cv2.addWeighted(data, 0.2, image, 0.9, gamma=0)
    # print(data.shape, image.shape)
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def highlight_labels(img, labels, maskimg=None):
    # create a copy of the image so we can draw on it
    imgcpy = img.copy()

    # draw a quadrilateral for each label in red
    cv2.polylines(imgcpy, labels, True, (0, 0, 255), thickness=2)

    # a mask needs to be created from the labels so we can properly highlight.
    # if the mask isn't passed in, we create it
    if type(maskimg) == type(None):
        maskimg = np.zeros(imgcpy.shape, dtype=np.float)
        for label in labels:
            # this sets all pixels inside the label to 1
            cv2.fillConvexPoly(maskimg, label, (1))

            # create a rgb version of the mask by setting each channel to the mask we created
    maskimg = (maskimg > 0).astype(np.uint8)
    maskrgb = np.zeros(imgcpy.shape, np.uint8)
    maskrgb[:, :, 0] = maskimg
    maskrgb[:, :, 1] = maskimg
    maskrgb[:, :, 2] = maskimg

    # interpolate image with white using a weighted sum
    bgimg = .5 * 255 * np.ones(imgcpy.shape, np.float) + .5 * imgcpy.astype(np.float)
    # mask out the background
    bgimg = (1 - maskrgb) * bgimg
    # cast to uint8 image
    bgimg = np.round(bgimg).astype(np.uint8)

    # get foreground unchanged
    fgimg = maskrgb * imgcpy

    # add white tinted background with unchanged foreground
    imgcpy = bgimg + fgimg.astype(np.uint8)

    return imgcpy

if __name__ == '__main__':
    # det = Detector(detector_name='yolo', config_path='config.cfg')
    det = Detector(detector_name='mobilenet_ssd', config_path='config.cfg')
    #
    #
    # # detect_image_test
    # # start = time.time()
    # path_imgs = ['1.jpg', '2.jpg', '2.png', '3.png', '8620050-4x3-940x705.jpg', 'back.jpg', 'tet.jpg']
    results = det.detect_image('test.jpeg', is_display=False)
    cs = np.array([[(result[1] + result[3])/2, (result[2] + result[4])/2] for result in results])
    # heatmap(cs, cv2.imread('test.jpeg'))
    # print(results)
    labels = [[[result[1], result[2]], [result[1], result[4]], [result[3], result[4]], [result[3], result[2]]] for result in results]
    labels = np.array(labels)
    frame = cv2.imread('test.jpeg')
    accumulated_exposures = np.zeros((frame.shape[0], frame.shape[1]), dtype=np.float)
    masking = np.zeros(accumulated_exposures.shape, dtype=np.float)
    for label in labels:
        # print(label)
        h, w = label[2][1] - label[0][1], label[2][0]-label[0][0]
        masking[label[0][1]:label[2][1], label[0][0]:label[2][0]] += 0.1
        masking[label[0][1]+int(h/8):label[2][1]-int(h/8), label[0][0]+int(w/8):label[2][0]-int(w/8)] += 0.1
        masking[label[0][1]+int(h/4):label[2][1]-int(h/4), label[0][0]+int(w/4):label[2][0]-int(w/4)] += 0.1
        masking[label[0][1]+int(3*h/8):label[2][1]-int(3*h/8), label[0][0]+int(3*w/8):label[2][0]-int(3*w/8)] += 0.1
        # masking[label[0][1]+int(h/8):label[2][1]-50, label[0][0]+50:label[2][0]-50] += 0.3
        # cv2.fillConvexPoly(masking, label, (1.0))
    # highlighted_image = highlight_labels(frame, labels, maskimg)
    # cv2.imwrite('test_out.jpeg', highlighted_image)
    #
    # cv2.imshow('image', masking)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # # accumulate the heatmap object exposure time
    accumulated_exposures = accumulated_exposures + masking
    data = np.array(accumulated_exposures)
    # cv2.destroyAllWindows()
    # # create the figure
    # print(accumulated_exposures.shape)
    canvas = np.uint8(cm.jet(data)*255)
    canvas = cv2.cvtColor(canvas, cv2.COLOR_RGBA2BGR)
    # cv2.imwrite("heatmap.png", canvas)
    image = cv2.imread('test.jpeg')
    img = cv2.addWeighted(canvas, 0.4, image, 0.9, gamma=0)
    cv2.imshow('image', img)
    cv2.waitKey(0)
    # fig, axis = plt.subplots()
    # # # set the colormap - there are many options for colormaps - see documentation
    # # # we will use cm.jet
    # hm = axis.pcolor(data, cmap="Blues_r")
    # # # set axis ranges
    # axis.set(xlim=[0, data.shape[1]], ylim=[0, data.shape[0]], aspect=1)
    # # # need to invert coordinate for images
    # axis.invert_yaxis()
    # # # remove the ticks
    # axis.set_xticks([])
    # axis.set_yticks([])
    # #
    # # # fit the colorbar to the height
    # shrink_scale = 1.0
    # aspect = data.shape[0] / float(data.shape[1])
    # if aspect < 1.0:
    #     shrink_scale = aspect
    # clb = plt.colorbar(hm, shrink=shrink_scale)
    # # set title
    # clb.ax.set_title('Exposure (seconds)', fontsize=10)
    # # plt.imshow()
    # # clb.ax.imshow()
    # # clb.imshow()
    # # plt.show()
    # # saves image to same directory that the script is located in (our working directory)
    # plt.savefig('heatmap.png', bbox_inches='tight')
    # # # close objects
    # plt.close('all')
    # det.detect_image('8620050-4x3-940x705.jpg', is_display=True)
    # print(time.time() - start)

    # detect_video_test
    # det.detect_video('Pedestrian_detector.mp4')

    # detect_webcam_test
    #det.detect_webcam()

    # det = Detector(detector_name='mobilenet', config_path='detectors.cfg')
    #det.detect_webcam()

    # Detect image sequence

