import numpy as np
from counter_algorithms.treeset import TreeSet

def binary_search(a, l, h, x):
    while l < h:
        mid = l + (h - l) // 2
        if a[mid] < x:
            l = mid + 1
        else:
            h = mid - 1

    return l-1

def onSegment(p, q, r):
    if q[0] <= max(p[0], r[0]) and q[0] >= min(p[0], r[0]) \
            and q[1] <= max(p[1], r[1]) and q[1] >= min(p[1], r[1]):
        return True
    return False


def orientation(p, q, r):
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
    if val == 0: return 0
    if val > 0: return 1
    return 2

def do_intersect(p1, q1, p2, q2):
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    if (o1 != o2 and o3 != o4):
        return True
    if (o1 == 0 and onSegment(p1, p2, q1)):
        return True
    if (o2 == 0 and onSegment(p1, q2, q1)):
        return True
    if (o3 == 0 and onSegment(p2, p1, q2)):
        return True
    if (o4 == 0 and onSegment(p2, q1, q2)):
        return True
    return False


def is_inside(polygon, n, p):
    if n < 3:
        return False
    extreme = [-99999999, p[1]]
    count, i = 0, 0
    while True:
        next = (i+1)%n
        if do_intersect(polygon[i], polygon[next], p, extreme):
            if orientation(polygon[i], p, polygon[next]) == 0:
                return onSegment(polygon[i], p, polygon[next])
            count += 1
        i = next
        if i == 0:
            break
    return count & 1


def quadTree(data, polygon, n, count, p1, p2, threshold=2):
    if is_inside(polygon, n, p1) == 0 and is_inside(polygon, n, p1) == 0:
        return
    if is_inside(polygon, n, p1) == 1 and is_inside(polygon, n, p1) == 1:
        count += get_data(data, p1[0]//2, p1[1]//2, p2[0]//2, p2[1]//2)
        return
    if p2[0] - p1[0] <= threshold and p2[1] - p1[1] <= threshold:
        count += data[int(p1[0]/2)][int(p1[1]/2)]
        return

    if p2[0] - p1[0] <= threshold:
        quadTree(data, polygon, n, p1, [p2[0], int((p1[1] + p2[1])/2)])
        quadTree(data, polygon, n, [p1[0], int((p1[1] + p2[1]) / 2)], p2)
        return
    if p2[1] - p1[1] <= threshold:
        quadTree(data, polygon, n, p1, [int((p1[0] + p2[0]) / 2), p2[1]])
        quadTree(data, polygon, n, [int((p1[0] + p2[0]) / 2), p1[1]])
        return

    quadTree(data, polygon, n, p1, [int((p1[0] + p2[0]) / 2), int((p1[1] + p2[1]) / 2)])
    quadTree(data, polygon, n, [int((p1[0] + p2[0]) / 2), int((p1[1] + p2[1]) / 2)], p2)
    quadTree(data, polygon, n, [p1[0], int((p1[1] + p2[1]) / 2)], [int((p1[0] + p2[0]) / 2), p2[1]])
    quadTree(data, polygon, n, [int((p1[0] + p2[0]) / 2), p1[1]], [p2[0], int((p1[1] + p2[1]) / 2)])

def get_data(data, i1, j1, i2, j2):

    return data[i2][j2] - data[i1][j2] - data[i2][j1] + data[i1-1][j1-1]

def implement(boxes, w=1080, h=720, nVertical_grid=108, nHorizital_grid=72, polygon=None):
    """
    :param boxes:[x1, y1, x2, y2, id] list
    :param w:
    :param h:
    :param nVertical_grid:
    :param nHorizital_grid:
    :param polygon:
    :return:
    """
    #intialization
    human_data = [[[]] * nVertical_grid] * nHorizital_grid
    xx = np.linspace(0, w, nVertical_grid+1)
    yy = np.linspace(0, h, nHorizital_grid+1)
    centre_points = [[(box[0] + box[2])/2, (box[1] + box[3])/2, box[4]] for box in boxes]
    for point in centre_points:
        index_x = binary_search(xx, 0, len(xx), point[0])
        index_y = binary_search(yy, 0, len(yy), point[1])
        human_data[index_y][index_x].append(point[2])


    retrival_data = [[[]] * nVertical_grid] * nHorizital_grid
    # # print(np.array(retrival_data).shape)
    retrival_data[0][0] = human_data[0][0]
    for i in range(1, nHorizital_grid):
        retrival_data[i][0] = retrival_data[i-1][0]
        retrival_data[i][0].extend(human_data[i][0])
        print(retrival_data[i][0])
        # break
        # for j in human_data[i][0]:
        #     retrival_data[i][0].append(j)
    #
    # for j in range(1, nVertical_grid):
    #     retrival_data[0][j] = retrival_data[0][j-1]
    #     retrival_data[0][j].extend(human_data[0][j])
    #
    # for i in range(1, nHorizital_grid):
    #     for j in range(1, nVertical_grid):
    #         retrival_data[i][j] = retrival_data[i-1][j]
    #         retrival_data[i][j].extend(retrival_data[i][j-1])
    #         for k in retrival_data[i-1][j-1]:
    #             retrival_data[i][j].remove(k)



if __name__ == "__main__":
    implement(np.random.randint(0, 64, [10, 5]))
    # print(np.random.randint(0, 64, [10, 5]))
    # print(binary_search([2, 3, 4, 10, 40], 0, 5, 6))