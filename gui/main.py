# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(772, 508)
        self.starting_btn = QtWidgets.QPushButton(Dialog)
        self.starting_btn.setGeometry(QtCore.QRect(570, 460, 181, 27))
        self.starting_btn.setObjectName("starting_btn")
        self.detector_cbb = QtWidgets.QComboBox(Dialog)
        self.detector_cbb.setGeometry(QtCore.QRect(670, 420, 91, 31))
        self.detector_cbb.setObjectName("detector_cbb")
        self.detector_cbb.addItem("")
        self.detector_cbb.addItem("")
        self.tracker_cbb = QtWidgets.QComboBox(Dialog)
        self.tracker_cbb.setGeometry(QtCore.QRect(570, 420, 81, 31))
        self.tracker_cbb.setObjectName("tracker_cbb")
        self.tracker_cbb.addItem("")
        self.tracker_cbb.addItem("")
        self.tracker_cbb.addItem("")
        self.video_display_fr = QtWidgets.QFrame(Dialog)
        self.video_display_fr.setGeometry(QtCore.QRect(-20, 0, 571, 511))
        self.video_display_fr.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.video_display_fr.setFrameShadow(QtWidgets.QFrame.Raised)
        self.video_display_fr.setObjectName("video_display_fr")
        self.graphicsView = QtWidgets.QGraphicsView(self.video_display_fr)
        self.graphicsView.setGeometry(QtCore.QRect(10, 0, 561, 31))
        self.graphicsView.setObjectName("graphicsView")
        self.comboBox = QtWidgets.QComboBox(Dialog)
        self.comboBox.setGeometry(QtCore.QRect(570, 370, 79, 27))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.dateEdit = QtWidgets.QDateEdit(Dialog)
        self.dateEdit.setGeometry(QtCore.QRect(620, 30, 110, 28))
        self.dateEdit.setObjectName("dateEdit")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def mytest(self):
        print("Mytest module 1")



    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.starting_btn.setText(_translate("Dialog", "Tracking"))
        self.detector_cbb.setItemText(0, _translate("Dialog", "deep sort"))
        self.detector_cbb.setItemText(1, _translate("Dialog", "sort"))
        self.tracker_cbb.setItemText(0, _translate("Dialog", "mobileNet_SSD"))
        self.tracker_cbb.setItemText(1, _translate("Dialog", "Yolov3_tiny"))
        self.tracker_cbb.setItemText(2, _translate("Dialog", "faceboxes"))
        self.comboBox.setItemText(0, _translate("Dialog", "gcrp://127.62.167"))




