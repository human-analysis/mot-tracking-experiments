import sys
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.QtCore import pyqtSignal, QObject
from gui.dialog import Ui_Dialog


class Communicate(QObject):
    closeApp = pyqtSignal()

class AppWindow(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.starting_btn.clicked.connect(self.buttonClick)
        self.show()

    def buttonClick(self):
        print(11111111111111)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = AppWindow()
    window.show()
    sys.exit(app.exec_())